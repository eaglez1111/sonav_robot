#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image

def thetaS_callback(im):
    print im.header, im.height, im.width, im.encoding, im.is_bigendian, im.step
    print type(im.data)
    print len(im.data)
    print len(im.data[0])
    print (im.data[0])
    print (im.data)
    while(1):
        pass

def main():
    rospy.init_node('rectifier', anonymous=True)

    pub = rospy.Publisher('chatter', Image, queue_size=10)
    rate = rospy.Rate(10) # 10hz

    rospy.Subscriber('/theta_s_uvc/image_raw', Image, thetaS_callback)

    msg = Image()
    rospy.spin()
    '''
    while not rospy.is_shutdown():
        msg.id = msg.id+1
        msg.st="hello world ***"+str(msg.id)
        rospy.loginfo(msg)
        print type(msg.a)
        pub.publish(msg)
        rate.sleep()
    '''

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
