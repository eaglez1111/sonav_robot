#!/usr/bin/env python
import rospy
import numpy as np
import cv2
import imutils
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


cnt=0

bridge = CvBridge()
cam0_pub = rospy.Publisher('/cam0/image_raw', Image, queue_size=10)
cam1_pub = rospy.Publisher('/cam1/image_raw', Image, queue_size=10)

def thetaS_callback(data):
    global cnt
    # print "INFO!!"
    # print im.header, im.height, im.width, im.encoding, im.is_bigendian, im.step
    # print type(im.data)
    # print len(im.data)
    # print len(im.data[0])
    # print "data!"
    # print (im.data[0:5])
    #print (im.data)

    # cv2.imshow('v0', im[:,:1280/2])
    # cv2.waitKey(0)
    # cv2.imshow('v1', im[:,1280/2:])
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    im = bridge.imgmsg_to_cv2(data, "rgb8")[:640,:]
    cv2.imwrite("/home/liquid/proj/fisheyeStitcher/images/s"+str(cnt)+".png", cv2.cvtColor(im, cv2.COLOR_BGR2RGB))


    im0 = bridge.cv2_to_imgmsg(im[:,:640], "rgb8")
    im0.header = data.header
    cam0_pub.publish(im0)

    #cv2.imshow('v1', im[:,1280/2:])
    #if cv2.waitKey(1) & 0xFF == ord('c'):
    cvim0 = imutils.rotate(im[:,:1280/2], 270-90)
    cv2.imwrite("/home/liquid/proj/fisheyeStitcher/images/cb0/"+str(cnt)+".png", cv2.cvtColor(cvim0, cv2.COLOR_BGR2RGB))


    im1 = bridge.cv2_to_imgmsg(im[:,640:], "rgb8")
    im1.header = data.header
    cam1_pub.publish(im1)
    cvim1 = imutils.rotate(im[:,1280/2:], 90-90)
    cv2.imwrite("/home/liquid/proj/fisheyeStitcher/images/cb1/"+str(cnt)+".png", cv2.cvtColor(cvim1, cv2.COLOR_BGR2RGB))

    cvim = np.concatenate((cvim0, cvim1), axis=0)
    cvim = imutils.rotate_bound(cvim, -90)
    cv2.imwrite("/home/liquid/proj/fisheyeStitcher/images/"+str(cnt)+".png", cv2.cvtColor(cvim, cv2.COLOR_BGR2RGB))

    cnt+=1
    while(1): pass

def main():
    rospy.init_node('rectifier', anonymous=True)

    rate = rospy.Rate(10) # 10hz

    rospy.Subscriber('/theta_s_uvc/image_raw', Image, thetaS_callback)

    msg = Image()

    rospy.spin()
    '''
    while not rospy.is_shutdown():
        msg.id = msg.id+1
        msg.st="hello world ***"+str(msg.id)
        rospy.loginfo(msg)
        print type(msg.a)
        pub.publish(msg)
        rate.sleep()
    '''

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
