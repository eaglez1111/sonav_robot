# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "temp0: 1 messages, 0 services")

set(MSG_I_FLAGS "-Itemp0:/home/liquid/proj/sonav_robot/ws0/src/temp0/msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(temp0_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg" NAME_WE)
add_custom_target(_temp0_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "temp0" "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(temp0
  "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/temp0
)

### Generating Services

### Generating Module File
_generate_module_cpp(temp0
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/temp0
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(temp0_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(temp0_generate_messages temp0_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg" NAME_WE)
add_dependencies(temp0_generate_messages_cpp _temp0_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(temp0_gencpp)
add_dependencies(temp0_gencpp temp0_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS temp0_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(temp0
  "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/temp0
)

### Generating Services

### Generating Module File
_generate_module_eus(temp0
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/temp0
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(temp0_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(temp0_generate_messages temp0_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg" NAME_WE)
add_dependencies(temp0_generate_messages_eus _temp0_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(temp0_geneus)
add_dependencies(temp0_geneus temp0_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS temp0_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(temp0
  "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/temp0
)

### Generating Services

### Generating Module File
_generate_module_lisp(temp0
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/temp0
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(temp0_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(temp0_generate_messages temp0_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg" NAME_WE)
add_dependencies(temp0_generate_messages_lisp _temp0_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(temp0_genlisp)
add_dependencies(temp0_genlisp temp0_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS temp0_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(temp0
  "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/temp0
)

### Generating Services

### Generating Module File
_generate_module_nodejs(temp0
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/temp0
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(temp0_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(temp0_generate_messages temp0_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg" NAME_WE)
add_dependencies(temp0_generate_messages_nodejs _temp0_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(temp0_gennodejs)
add_dependencies(temp0_gennodejs temp0_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS temp0_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(temp0
  "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/temp0
)

### Generating Services

### Generating Module File
_generate_module_py(temp0
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/temp0
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(temp0_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(temp0_generate_messages temp0_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/liquid/proj/sonav_robot/ws0/src/temp0/msg/Num.msg" NAME_WE)
add_dependencies(temp0_generate_messages_py _temp0_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(temp0_genpy)
add_dependencies(temp0_genpy temp0_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS temp0_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/temp0)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/temp0
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(temp0_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/temp0)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/temp0
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(temp0_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/temp0)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/temp0
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(temp0_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/temp0)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/temp0
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(temp0_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/temp0)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/temp0\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/temp0
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(temp0_generate_messages_py std_msgs_generate_messages_py)
endif()
