set(_CATKIN_CURRENT_PACKAGE "theta_s_uvc")
set(theta_s_uvc_VERSION "0.0.0")
set(theta_s_uvc_MAINTAINER "root <root@todo.todo>")
set(theta_s_uvc_PACKAGE_FORMAT "1")
set(theta_s_uvc_BUILD_DEPENDS "roscpp" "rospy" "sensor_msgs" "std_msgs")
set(theta_s_uvc_BUILD_EXPORT_DEPENDS "roscpp" "rospy" "sensor_msgs" "std_msgs")
set(theta_s_uvc_BUILDTOOL_DEPENDS "catkin")
set(theta_s_uvc_BUILDTOOL_EXPORT_DEPENDS )
set(theta_s_uvc_EXEC_DEPENDS "roscpp" "rospy" "sensor_msgs" "std_msgs")
set(theta_s_uvc_RUN_DEPENDS "roscpp" "rospy" "sensor_msgs" "std_msgs")
set(theta_s_uvc_TEST_DEPENDS )
set(theta_s_uvc_DOC_DEPENDS )
set(theta_s_uvc_URL_WEBSITE "")
set(theta_s_uvc_URL_BUGTRACKER "")
set(theta_s_uvc_URL_REPOSITORY "")
set(theta_s_uvc_DEPRECATED "")