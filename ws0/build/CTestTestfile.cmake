# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/ws0/src
# Build directory: /home/liquid/proj/sonav_robot/ws0/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(temp0)
subdirs(rgb360)
subdirs(image_undistort)
subdirs(theta_s_uvc)
