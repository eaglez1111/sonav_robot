// Auto-generated. Do not edit!

// (in-package temp0.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Num {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.id = null;
      this.st = null;
      this.a = null;
    }
    else {
      if (initObj.hasOwnProperty('id')) {
        this.id = initObj.id
      }
      else {
        this.id = 0;
      }
      if (initObj.hasOwnProperty('st')) {
        this.st = initObj.st
      }
      else {
        this.st = '';
      }
      if (initObj.hasOwnProperty('a')) {
        this.a = initObj.a
      }
      else {
        this.a = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Num
    // Serialize message field [id]
    bufferOffset = _serializer.int64(obj.id, buffer, bufferOffset);
    // Serialize message field [st]
    bufferOffset = _serializer.string(obj.st, buffer, bufferOffset);
    // Serialize message field [a]
    bufferOffset = _arraySerializer.uint16(obj.a, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Num
    let len;
    let data = new Num(null);
    // Deserialize message field [id]
    data.id = _deserializer.int64(buffer, bufferOffset);
    // Deserialize message field [st]
    data.st = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [a]
    data.a = _arrayDeserializer.uint16(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.st.length;
    length += 2 * object.a.length;
    return length + 16;
  }

  static datatype() {
    // Returns string type for a message object
    return 'temp0/Num';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'da1da67fd9820aac4058cae5eb7f8122';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int64 id
    string st
    uint16[] a
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Num(null);
    if (msg.id !== undefined) {
      resolved.id = msg.id;
    }
    else {
      resolved.id = 0
    }

    if (msg.st !== undefined) {
      resolved.st = msg.st;
    }
    else {
      resolved.st = ''
    }

    if (msg.a !== undefined) {
      resolved.a = msg.a;
    }
    else {
      resolved.a = []
    }

    return resolved;
    }
};

module.exports = Num;
