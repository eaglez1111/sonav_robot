;; Auto-generated. Do not edit!


(when (boundp 'temp0::Num)
  (if (not (find-package "TEMP0"))
    (make-package "TEMP0"))
  (shadow 'Num (find-package "TEMP0")))
(unless (find-package "TEMP0::NUM")
  (make-package "TEMP0::NUM"))

(in-package "ROS")
;;//! \htmlinclude Num.msg.html


(defclass temp0::Num
  :super ros::object
  :slots (_id _st _a ))

(defmethod temp0::Num
  (:init
   (&key
    ((:id __id) 0)
    ((:st __st) "")
    ((:a __a) (make-array 0 :initial-element 0 :element-type :integer))
    )
   (send-super :init)
   (setq _id (round __id))
   (setq _st (string __st))
   (setq _a __a)
   self)
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:st
   (&optional __st)
   (if __st (setq _st __st)) _st)
  (:a
   (&optional __a)
   (if __a (setq _a __a)) _a)
  (:serialization-length
   ()
   (+
    ;; int64 _id
    8
    ;; string _st
    4 (length _st)
    ;; uint16[] _a
    (* 2    (length _a)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64 _id
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _id (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _id) (= (length (_id . bv)) 2)) ;; bignum
              (write-long (ash (elt (_id . bv) 0) 0) s)
              (write-long (ash (elt (_id . bv) 1) -1) s))
             ((and (class _id) (= (length (_id . bv)) 1)) ;; big1
              (write-long (elt (_id . bv) 0) s)
              (write-long (if (>= _id 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _id s)(write-long (if (>= _id 0) 0 #xffffffff) s)))
     ;; string _st
       (write-long (length _st) s) (princ _st s)
     ;; uint16[] _a
     (write-long (length _a) s)
     (dotimes (i (length _a))
       (write-word (elt _a i) s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64 _id
#+(or :alpha :irix6 :x86_64)
      (setf _id (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _id (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; string _st
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _st (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; uint16[] _a
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _a (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _a i) (sys::peek buf ptr- :short)) (incf ptr- 2)
     ))
   ;;
   self)
  )

(setf (get temp0::Num :md5sum-) "da1da67fd9820aac4058cae5eb7f8122")
(setf (get temp0::Num :datatype-) "temp0/Num")
(setf (get temp0::Num :definition-)
      "int64 id
string st
uint16[] a

")



(provide :temp0/Num "da1da67fd9820aac4058cae5eb7f8122")


