#!/usr/bin/env python

# Dapeng Zhao (Eagle)
# dapengz@andrew.cmu.edu or tim.eagle.zhao@gmail.com

import numpy as np
import Odrv

''' Constant '''
cos60 = np.cos(np.pi/180*60)
sin60 = np.sin(np.pi/180*60)


class Omni3:

    def __init__(self):
        self.ports = ['/dev/ttyOd0','/dev/ttyOd1']
        self.M=[0,1,2]
        self.mpc = 0.102*np.pi/8192
        self.R = 22.5
        self.vxyw_v012 = np.array([[-cos60,-sin60,-self.R],[-cos60,sin60,-self.R],[1,0,-self.R]])

    def fetch(self):
        self.Od = Odrv.fetchOd(self.ports)
        if len(self.Od)==2:
            print self.getErrRdy()
            return True
        return False

    def getErrRdy(self):
        Err, Rdy = Odrv.readErrRdy(self.Od,self.M)
        #if not Rdy[1] and Rdy[3]: self.Od = [self.Od[1],self.Od[0]] # Make sure the empty motor channel is at Odrv1.M1 . This line is not necessary now with renaming mechanism: https://unix.stackexchange.com/questions/66901/how-to-bind-usb-device-under-a-static-name
        return Err, Rdy

    def clearErr(self):
        return Odrv.writeMode(self.Od,self.M,[2,2,2])

    def reboot(self):
        return Odrv.rebootAll(self.Od)

    def activate(self,feedback_control=1):
        Ctrl = [feedback_control!=0]*3
        return Odrv.writeMode(self.Od,self.M,Ctrl)

    def move(self,vx,vy,w):
        V_xyw = np.array([1.0*vx,vy,w])
        return self.writeVel( self.vxyw_v012.dot(V_xyw) )


    def writeVel(self,Vel): # Input linear velocity , write rotational speed to odrive
        Spd = np.array(Vel)/self.mpc
        return Odrv.writeSpd( self.Od,self.M,Spd )





def r():
    base.reboot()

if __name__ == '__main__':
    base = Omni3()
    base.fetch()

#
