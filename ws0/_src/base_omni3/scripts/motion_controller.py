#!/usr/bin/env python

# EagleZ, tim.eagle.zhao@gmail.com or dapengz@andrew
# Oct 2019

'''ROS Imports'''
import rospy
from geometry_msgs.msg import Twist
from robot_config.msg import BaseStatus
#from robot_config.msg import BatteryStatus   # not implemented yet
#from robot_config.msg import BaseCmd   # not used


'''Functional Imports'''
from Omni3 import Omni3
import numpy as np


'''Parameter'''
freq = 20 #Hz


'''Global Varaible'''
base = None


'''Subscriber Routine'''
def controller(data):
    base.move(data.linear.x, 0, data.angular.z)


'''Main()'''
def main():

    base = Omni3()
    base.fetch()
    base.activate()

    rospy.init_node('motion_controller', anonymous=True)
    rospy.Subscriber('cmd_vel', Twist, controller)

    ros.spin()

'''Run'''
if __name__ == '__main__':
    main()




#
