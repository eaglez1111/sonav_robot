# https://stackoverflow.com/questions/46557583/how-to-identify-which-button-is-being-pressed-on-ps4-controller-using-pygame

from Omni3 import Omni3
import time
import numpy as np
import pygame

eps = 10**-3
JS = [0,0,2,1,1,2]
AX = [0,1,0,0,1,1]
VB = [0,0,1,0,0,1]
VS = [1,-1,0.5,1,-1,0.5]
MX= [-2,-2,0,-2,-2,0]
printStatus = 0

class PS4_Controller:
    def init(self):
        pygame.init()
        self.j = pygame.joystick.Joystick(0)
        self.j.init()
        self.button = [False]*17
        self.joystk = [[0,0],[0,0],[0,0]]
        self.joystk_last = [[0,0],[0,0],[0,0]]
    def update(self):
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.JOYAXISMOTION:
                if event.value != 0:
                    if printStatus: print(event.dict, event.joy, event.axis, event.value)
                    i = event.axis
                    self.joystk[JS[i]][AX[i]] = max(MX[i],(event.value+VB[i])*VS[i])
            elif event.type == pygame.JOYBUTTONDOWN:
                if printStatus: print(event.dict, event.joy, event.button, 'pressed')
                self.button[event.button] = True
            elif event.type == pygame.JOYBUTTONUP:
                if printStatus: print(event.dict, event.joy, event.button, 'released')
                self.button[event.button] = False
            elif event.type == pygame.JOYHATMOTION:
                if printStatus: print(event.dict, event.joy, event.hat, event.value)
                self.button[13] = event.value[1]==-1
                self.button[14] = event.value[0]==1
                self.button[15] = event.value[1]==1
                self.button[16] = event.value[0]==-1
    def resetJS(self):
        #self.joystk=[[0,0],[0,0],[0,0]]
        for i in range(3):
            for j in range(2):
                #if abs(self.joystk[i][j]-self.joystk_last[i][j]) > eps :
                if self.joystk[i][j]!=self.joystk_last[i][j]:
                    self.joystk_last[i][j] = self.joystk[i][j]
                else:
                    self.joystk[i][j] = 0

base = Omni3()
base.fetch()
ctrlr = PS4_Controller()
ctrlr.init()
Vel_Scale = np.array([0.0, 0.5, 1.0, 1.5])*1
try:
    while(1):
        ctrlr.update()
        #ctrlr.resetJS()
        print np.array(ctrlr.button).astype(int)
        print ctrlr.joystk[0]
        print ctrlr.joystk[1]
        print ctrlr.joystk[2]
        time.sleep(0.01)

        if ctrlr.button[14]:
            base.reboot()
            while(1):
                pass
        if ctrlr.button[15]:
            base.clearErr()
        if ctrlr.button[16]:
            base.reboot()
            time.sleep(3)
            base.fetch()
            ctrlr.init()
            time.sleep(1)

        if ctrlr.button[0]:
            base.activate(0)
            #time.sleep(0.5)
            print 'Stop...'
        elif ctrlr.button[1]:
            base.activate(1)
            #time.sleep(0.5)
            print 'Act!'


        vs = Vel_Scale[ctrlr.button[4]+ctrlr.button[5]*2]
        base.move(vs*ctrlr.joystk[1][0],vs*ctrlr.joystk[1][1],vs*ctrlr.joystk[0][0]*0.02)


except KeyboardInterrupt:
    print("EXITING NOW")
    ctrlr.j.quit()
