#!/usr/bin/env python


# Dapeng Zhao (Eagle)
# dapengz@andrew.cmu.edu or tim.eagle.zhao@gmail.com

# For more information about the numbers in this program:
# https://github.com/madcowswe/ODrive/blob/master/tools/odrive/enums.py OR contact EagleZ

# How to rename usb ports
# https://unix.stackexchange.com/questions/66901/how-to-bind-usb-device-under-a-static-name


import serial
import itertools
import time
import numpy as np


pause_between_commands = 0.001 #sec
serialReading_timeout = 0.01 #sec
serialWriting_timeout = 0.01 #sec
use_checksum = False
PRINTDEBUG = False


def fetchOd(ports):
    Od = []
    for port in ports:
        try:
            od=serial.Serial(port, 115200,
                             timeout=serialReading_timeout,
                             write_timeout=serialWriting_timeout)
        except:
            print "Check Odrive connection:", port, "fetching failed! "
            time.sleep(1)
        else:
            print "Odrve connected as ",od.name," !"
            Od.append(od)
    return Od


def writeMode(Od,M,Ctrl):
    M, Ctrl = makeList(M), makeList(Ctrl)
    result = 1
    for m, ctrl in itertools.izip(M, Ctrl):
        od_i, ch = int(m/2), int(m%2)
        ctrlCmd = {
            -1: 'sr', # reboot the whole Odrive controller, reboot is allowed to by triggered by either channel(/motor)
            0 : 'w axis'+str(ch)+'.requested_state 1', # Deactivate feedback loop control
            1 : 'w axis'+str(ch)+'.requested_state 8', # Activate feedback loop control
            2 : 'w axis'+str(ch)+'.error 0' # clear error
        }
        result *= od_write( Od[od_i], ctrlCmd.get(ctrl,'') )
    return result


def writeSpd(Od,M,Spd):
    M, Spd = makeList(M), makeList(Spd)
    result = 1
    for m, spd in itertools.izip(M, Spd):
        od_i, ch = int(m/2), int(m%2)
        spd_s = str(spd)[:7]
        cmd = 'v '+str(ch)+' '+spd_s
        result *= od_write(Od[od_i],cmd)
        if PRINTDEBUG: print cmd, result
    return result


def readStatus(Od,M):
    M = makeList(M)
    Dis, Spd = [], []
    for m in M:
        od_i, ch = int(m/2), int(m%2)
        try:
            dis, spd = getFloats(getResponse(Od[od_i],"f "+str(ch)))
        except:
            dis, spd = None, None
            print "Reading status: motor",m,"failed!"
        Dis.append(dis)
        Spd.append(spd)
    return Dis, Spd


def readErrRdy(Od,M):
    M = makeList(M)
    Err, Rdy = [], []
    for m in M:
        od_i, ch = int(m/2), int(m%2)
        try:
            err = int(getResponse(Od[od_i],'r axis'+str(ch)+'.error'))
        except:
            err = 111 # because 111 is not a Odrive error code, so it s used to indicate reading failure
            print "Reading Err: motor",m,"failed!"
        try:
            rdy = int(getResponse(Od[od_i],'r axis'+str(ch)+'.encoder.is_ready'))
        except:
            rdy = 0
            print "Reading Readiness: motor",m,"failed!"
        Err.append(err)
        Rdy.append(rdy)
    return Err, Rdy


def rebootAll(Od):
    for od in Od:
        od.write('\nsr\n')
    return 1



''' Internal Functions '''

def od_write(od,st):
    if use_checksum:
        cmd = "{}*{}\n".format(st, checksum(st))
    else:
        cmd = "{}\n".format(st)
    length = len(cmd)
    try:
        if PRINTDEBUG: print cmd.rstrip()
        if (od.write(cmd)==length):
            return 1
    except:
        print "FAILED!!!! "+str(cmd)
    return 0


def getResponse(od, st, comment=None):
    result = od_write(od, st)

    time.sleep(pause_between_commands)
    line=od.readline().rstrip()
    if use_checksum:
        if not "*" in line:
            print "Response without checksum '{}'".format(line)
            return None

        print line
        res, cs = line.split("*")
        try:
            if checksum(res) != int(cs):
                print "checksum is different {} != {}".format(checksum(res), int(cs))
                return None
        except:
            print "checksum is not int"
            return None
    else:
        if PRINTDEBUG: print line
        res = line

    if comment:
        print "{}: {:20s} - written[{}]: {}, ".format(comment, res, result, st)
    return res


def getFloats(text):
    if text is None:
        return None, None
    items = text.split(" ")
    if len(items) < 2:
        print "Illegal input:'%s'"%(text)
        return None, None
    try:
        return float(items[0]), float(items[1])
    except:
        print "Illegal input:'%s'"%(text)
        return None, None


def checksum(st):
    cs = 0
    for c in st:
        cs = cs ^ ord(c)
    return cs & 0xff


def makeList(M):
    if type(M)!=type([]) and type(M)!=type(()) and type(M)!=type(np.array([])):
        M = [M]
    return M



def main():
    print('not implemented!')


if __name__ == '__main__':
    main()


#
