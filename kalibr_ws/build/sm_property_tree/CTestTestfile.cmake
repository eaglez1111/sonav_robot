# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/Schweizer-Messer/sm_property_tree
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/sm_property_tree
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_sm_property_tree_gtest_sm_property_tree-test "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_property_tree/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_property_tree/test_results/sm_property_tree/gtest-sm_property_tree-test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/sm_property_tree/lib/sm_property_tree/sm_property_tree-test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_property_tree/test_results/sm_property_tree/gtest-sm_property_tree-test.xml")
subdirs(gtest)
