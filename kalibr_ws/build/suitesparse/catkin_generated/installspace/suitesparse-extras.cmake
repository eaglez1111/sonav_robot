list(APPEND suitesparse_INCLUDE_DIRS ${suitesparse_DIR}/../../../include/suitesparse)


list(APPEND suitesparse_LIBRARIES 
  /home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/suitesparse/lib/libspqr${CMAKE_STATIC_LIBRARY_SUFFIX}
  /home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/suitesparse/lib/libcholmod${CMAKE_STATIC_LIBRARY_SUFFIX} 
  /home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/suitesparse/lib/libccolamd${CMAKE_STATIC_LIBRARY_SUFFIX} 
  /home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/suitesparse/lib/libcamd${CMAKE_STATIC_LIBRARY_SUFFIX} 
  /home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/suitesparse/lib/libcolamd${CMAKE_STATIC_LIBRARY_SUFFIX} 
  /home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/suitesparse/lib/libamd${CMAKE_STATIC_LIBRARY_SUFFIX} 
  lapack
  blas
  /home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/suitesparse/lib/libsuitesparseconfig${CMAKE_STATIC_LIBRARY_SUFFIX} )

if(NOT APPLE)
  list(APPEND suitesparse_LIBRARIES rt )
endif()
