# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_cv/aslam_cameras
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cameras
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_aslam_cameras_gtest_aslam_cameras_tests "/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cameras/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cameras/test_results/aslam_cameras/gtest-aslam_cameras_tests.xml" "--working-dir" "/home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_cv/aslam_cameras/test" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/aslam_cameras/lib/aslam_cameras/aslam_cameras_tests --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cameras/test_results/aslam_cameras/gtest-aslam_cameras_tests.xml")
subdirs(gtest)
