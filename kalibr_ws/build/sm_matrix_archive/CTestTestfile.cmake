# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/Schweizer-Messer/sm_matrix_archive
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/sm_matrix_archive
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_sm_matrix_archive_gtest_sm_matrix_archive-test "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_matrix_archive/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_matrix_archive/test_results/sm_matrix_archive/gtest-sm_matrix_archive-test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/sm_matrix_archive/lib/sm_matrix_archive/sm_matrix_archive-test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_matrix_archive/test_results/sm_matrix_archive/gtest-sm_matrix_archive-test.xml")
subdirs(gtest)
