# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_cv/aslam_cv_backend
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cv_backend
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_aslam_cv_backend_gtest_aslam_cv_backend_test "/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cv_backend/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cv_backend/test_results/aslam_cv_backend/gtest-aslam_cv_backend_test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/aslam_cv_backend/lib/aslam_cv_backend/aslam_cv_backend_test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cv_backend/test_results/aslam_cv_backend/gtest-aslam_cv_backend_test.xml")
subdirs(gtest)
