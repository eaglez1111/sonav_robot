# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_offline_calibration/kalibr
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/kalibr
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_kalibr_gtest_kalibr_test "/home/liquid/proj/sonav_robot/kalibr_ws/build/kalibr/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/kalibr/test_results/kalibr/gtest-kalibr_test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/kalibr/lib/kalibr/kalibr_test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/kalibr/test_results/kalibr/gtest-kalibr_test.xml")
subdirs(gtest)
