# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_offline_calibration/kalibr/include".split(';') if "/home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_offline_calibration/kalibr/include" != "" else []
PROJECT_CATKIN_DEPENDS = "aslam_cv_backend_python;aslam_backend_python;incremental_calibration_python;aslam_splines_python".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lkalibr_errorterms".split(';') if "-lkalibr_errorterms" != "" else []
PROJECT_NAME = "kalibr"
PROJECT_SPACE_DIR = "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/kalibr"
PROJECT_VERSION = "0.0.1"
