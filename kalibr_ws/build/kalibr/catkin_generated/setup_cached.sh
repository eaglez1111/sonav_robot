#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/kalibr:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/kalibr/lib:$LD_LIBRARY_PATH"
export PWD="/home/liquid/proj/sonav_robot/kalibr_ws/build/kalibr"
export PYTHONPATH="/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/kalibr/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/kalibr/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_offline_calibration/kalibr:$ROS_PACKAGE_PATH"