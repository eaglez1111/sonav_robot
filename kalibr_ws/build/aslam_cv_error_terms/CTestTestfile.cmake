# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_cv/aslam_cv_error_terms
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cv_error_terms
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_aslam_cv_error_terms_gtest_aslam_cv_error_terms_test "/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cv_error_terms/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cv_error_terms/test_results/aslam_cv_error_terms/gtest-aslam_cv_error_terms_test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/aslam_cv_error_terms/lib/aslam_cv_error_terms/aslam_cv_error_terms_test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_cv_error_terms/test_results/aslam_cv_error_terms/gtest-aslam_cv_error_terms_test.xml")
subdirs(gtest)
