# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/Schweizer-Messer/sm_common
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/sm_common
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_sm_common_gtest_sm_common-test "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_common/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_common/test_results/sm_common/gtest-sm_common-test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/sm_common/lib/sm_common/sm_common-test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_common/test_results/sm_common/gtest-sm_common-test.xml")
subdirs(gtest)
