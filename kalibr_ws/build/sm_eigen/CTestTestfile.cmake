# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/Schweizer-Messer/sm_eigen
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/sm_eigen
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_sm_eigen_gtest_sm_eigen-test "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_eigen/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_eigen/test_results/sm_eigen/gtest-sm_eigen-test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/sm_eigen/lib/sm_eigen/sm_eigen-test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_eigen/test_results/sm_eigen/gtest-sm_eigen-test.xml")
subdirs(gtest)
