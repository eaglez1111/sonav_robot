# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/liquid/proj/sonav_robot/kalibr_ws/install/include".split(';') if "/home/liquid/proj/sonav_robot/kalibr_ws/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "sm_eigen;aslam_backend".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lincremental_calibration".split(';') if "-lincremental_calibration" != "" else []
PROJECT_NAME = "incremental_calibration"
PROJECT_SPACE_DIR = "/home/liquid/proj/sonav_robot/kalibr_ws/install"
PROJECT_VERSION = "0.0.1"
