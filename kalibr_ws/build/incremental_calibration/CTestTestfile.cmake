# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_incremental_calibration/incremental_calibration
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/incremental_calibration
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_incremental_calibration_gtest_incremental_calibration_test "/home/liquid/proj/sonav_robot/kalibr_ws/build/incremental_calibration/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/incremental_calibration/test_results/incremental_calibration/gtest-incremental_calibration_test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/incremental_calibration/lib/incremental_calibration/incremental_calibration_test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/incremental_calibration/test_results/incremental_calibration/gtest-incremental_calibration_test.xml")
subdirs(gtest)
