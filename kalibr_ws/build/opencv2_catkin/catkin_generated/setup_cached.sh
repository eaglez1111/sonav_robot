#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/opencv2_catkin:$CMAKE_PREFIX_PATH"
export PWD="/home/liquid/proj/sonav_robot/kalibr_ws/build/opencv2_catkin"
export ROSLISP_PACKAGE_DIRECTORIES="/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/opencv2_catkin/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/opencv2_catkin:$ROS_PACKAGE_PATH"