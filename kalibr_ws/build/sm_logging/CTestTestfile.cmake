# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/Schweizer-Messer/sm_logging
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/sm_logging
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_sm_logging_gtest_sm_logging-test "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_logging/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_logging/test_results/sm_logging/gtest-sm_logging-test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/sm_logging/lib/sm_logging/sm_logging-test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_logging/test_results/sm_logging/gtest-sm_logging-test.xml")
subdirs(gtest)
