# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/Schweizer-Messer/sm_boost
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/sm_boost
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_sm_boost_gtest_sm_boost-test "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_boost/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_boost/test_results/sm_boost/gtest-sm_boost-test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/sm_boost/lib/sm_boost/sm_boost-test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_boost/test_results/sm_boost/gtest-sm_boost-test.xml")
subdirs(gtest)
