# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_nonparametric_estimation/aslam_splines
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_splines
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_aslam_splines_gtest_aslam_splines_test "/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_splines/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_splines/test_results/aslam_splines/gtest-aslam_splines_test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/aslam_splines/lib/aslam_splines/aslam_splines_test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/aslam_splines/test_results/aslam_splines/gtest-aslam_splines_test.xml")
subdirs(gtest)
