# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_optimizer/aslam_backend/include".split(';') if "/home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_optimizer/aslam_backend/include" != "" else []
PROJECT_CATKIN_DEPENDS = "sparse_block_matrix;sm_boost;sm_random;sm_timing;sm_logging;sm_property_tree".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-laslam_backend".split(';') if "-laslam_backend" != "" else []
PROJECT_NAME = "aslam_backend"
PROJECT_SPACE_DIR = "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/aslam_backend"
PROJECT_VERSION = "0.0.1"
