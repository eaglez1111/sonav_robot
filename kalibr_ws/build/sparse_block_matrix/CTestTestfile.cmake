# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/aslam_optimizer/sparse_block_matrix
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/sparse_block_matrix
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_sparse_block_matrix_gtest_sparse_block_matrix_tests "/home/liquid/proj/sonav_robot/kalibr_ws/build/sparse_block_matrix/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/sparse_block_matrix/test_results/sparse_block_matrix/gtest-sparse_block_matrix_tests.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/sparse_block_matrix/lib/sparse_block_matrix/sparse_block_matrix_tests --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/sparse_block_matrix/test_results/sparse_block_matrix/gtest-sparse_block_matrix_tests.xml")
subdirs(gtest)
