# CMake generated Testfile for 
# Source directory: /home/liquid/proj/sonav_robot/kalibr_ws/src/Kalibr/Schweizer-Messer/sm_timing
# Build directory: /home/liquid/proj/sonav_robot/kalibr_ws/build/sm_timing
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_sm_timing_gtest_sm_timing-test "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_timing/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_timing/test_results/sm_timing/gtest-sm_timing-test.xml" "--return-code" "/home/liquid/proj/sonav_robot/kalibr_ws/devel/.private/sm_timing/lib/sm_timing/sm_timing-test --gtest_output=xml:/home/liquid/proj/sonav_robot/kalibr_ws/build/sm_timing/test_results/sm_timing/gtest-sm_timing-test.xml")
subdirs(gtest)
